#ifndef DONNEE_H
#define DONNEE_H

#include "client.h"

using namespace std;

class Donnee:
			public Application
{
    public:
        Donnee();
        virtual ~Donnee();


        string Getcode() const { return code; }
        void Setcode(string val) { code = val; }

        int sendJSON();
        int sendJSON(int, string);

        //! \brief Methode de conversion des attributs de classe au format JSON
        //! \version 1.0
        //! \date 28 juillet 2023
        virtual string maDonneeToJSON() = 0;
        //! \brief Methode de mise a jour des attributs a partir d un JSON
        //! \version 1.0
        //! \date 28 juillet 2023
        virtual void updateWithJSON(string) = 0;
        //! \brief Methode d enregistrement dans un fichier log
        //! \version 1.0
        //! \date 28 juillet 2023
        virtual void logRecord() = 0;
        //! \brief Methode contenant l action principale de l objet
        //! \version 1.0
        //! \date 28 juillet 2023
        virtual void execute() = 0;

    protected:

    private:
        string code;
};

#endif // DONNEE_H
