#include "donnee.h"


//! \brief Constructeur de la classe abstraite Donnee
//! \version 1.0
//! \date 28 juillet 2023
//!
Donnee::Donnee()
{
}

//! \brief Destructeur de la classe abstraite Donnee
//! \version 1.0
//! \date 28 juillet 2023
//!
Donnee::~Donnee()
{
}

//! \brief Methode d'envoi au serveur au format JSON au Rover
//! \version 1.0
//! \date 28 juillet 2023
int Donnee::sendJSON()
{
    //cout << maDonneeToJSON() << endl;
    Client* ptrClient = new Client();
    ptrClient->order_to_bot(maDonneeToJSON());
    if(ptrClient!=nullptr)
    {
        delete ptrClient;
        ptrClient=nullptr;
    }
    return 0;
}

//! \brief Methode d'envoi au format JSON a un serveur de commandement
//! \fn int Donnee::sendJSON(int port, string addr)
//! \version 1.0
//! \date 28 juillet 2023
//! \param port de connexion et adresse IP du destinataire
//! \return reussite de l envoi du JSON au serveur
int Donnee::sendJSON(int port, string addr)
{
    //cout << maDonneeToJSON() << endl;
    Client* ptrClient = new Client(port, addr);
    ptrClient->order_to_bot(maDonneeToJSON());
    if(ptrClient!=nullptr)
    {
        delete ptrClient;
        ptrClient=nullptr;
    }
    return 0;
}
