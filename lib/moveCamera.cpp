#include "moveCamera.h"

moveCamera::moveCamera(int H, int V)
    :Donnee()
{
    Setcode("mvCam");
    SetcurrentInput((int)'G');
    SetangleH(H);
    SetangleV(V);
    angleToImp();
    execute();
    cout_mutex.lock();
    cout << "Construction de moveCamera: " << H << " , " << V << endl;
    cout_mutex.unlock();
}

moveCamera::moveCamera()
    :Donnee()
{
    Setcode("mvCam");
    SetcurrentInput((int)'G');
    this->sendJSON();
    cout_mutex.lock();
    cout << "Construction de moveCamera: " << endl;
    cout_mutex.unlock();
}

moveCamera::~moveCamera()
{
    cout_mutex.lock();
    cout << "Destruction de moveCamera" << endl;
    cout_mutex.unlock();
}

string moveCamera::maDonneeToJSON()
{
    // Conversion des angeles en tableau de char
    /* char horizontal[100];
    char vertical[100];
    gcvt(this->angleH, 4, horizontal);
    gcvt(this->angleV, 4, vertical);*/
    char input[100];
    gcvt(this->currentInput, 4, input);

    // Declaration de l'objet Json
    cJSON *root;
    /* Inititaliastion: create root node and array */
    root = cJSON_CreateObject();
    // Affectation des donn�es au Json
    cJSON_AddItemToObject(root, "Code", cJSON_CreateString(Getcode().c_str()));
    // cJSON_AddItemToObject(root, "Angle Horizontal", cJSON_CreateString(horizontal));
    // cJSON_AddItemToObject(root, "Angle Vertical", cJSON_CreateString(vertical));
    cJSON_AddItemToObject(root, "Input", cJSON_CreateString(input));

    // Mise en forme des donn�es au format Json et copie dans un string
    string ordres="";
    // cout << this->input << endl;
    ostringstream oss;
    oss << cJSON_Print(root);
    ordres = oss.str();
    return ordres;
}

//! \brief initialisation de la connexion i2c a la carte PCA9685
void moveCamera::setPWMFreq(int fd, int freq)
{
  int prescale = (int)(25000000.0 / 4096.0 / freq - 1.0);
  wiringPiI2CWriteReg8(fd, PCA9685_MODE1, 0x10); // sleep
  wiringPiI2CWriteReg8(fd, PCA9685_PRESCALE, prescale); // mise � l'�chelle
  wiringPiI2CWriteReg8(fd, PCA9685_MODE1, 0x80); // r�veil
  delay(5);
}

//! \brief envoi du signal "off" sur la pin "channel" de la carte PCA9685 "fd"
void moveCamera::setPWM(int fd, int channel, int on, int off)
{
  wiringPiI2CWriteReg8(fd, LED0_ON_L + 4*channel, on & 0xFF);
  wiringPiI2CWriteReg8(fd, LED0_ON_H + 4*channel, on >> 8);
  wiringPiI2CWriteReg8(fd, LED0_OFF_L + 4*channel, off & 0xFF);
  wiringPiI2CWriteReg8(fd, LED0_OFF_H + 4*channel, off >> 8);
}

//! \brief mise a l echelle entre l angle de camera souhaite et les impulsion a envoyer au servomoteur
void moveCamera::angleToImp()
{
  this->impV = (this->angleV-ANG_V_CEN)*(IMP_V_BAS-IMP_V_CEN) / (ANG_V_BAS-ANG_V_CEN) + IMP_V_CEN;
  this->impH = (this->angleH-ANG_H_GAU)*(IMP_H_DRO-IMP_H_GAU) / (ANG_H_DRO-ANG_H_GAU) + IMP_H_GAU;;
}

//! \brief deplacement de la camera sur les positions en cours de l objet instancie
void moveCamera::execute()
{
  if (this->impV < IMP_V_CEN || this->impV > IMP_V_BAS || this->impH < IMP_H_GAU || this->impH > IMP_H_DRO)
  {
    cout_mutex.lock();
    cout << "Consigne hors limite: " << this->impV << " , " << this->impH << endl;
    cout_mutex.unlock();
  }
  else
  {
  Setfd(wiringPiI2CSetup(PCA9685_I2C_ADDR));
  if (this->fd == -1)
  {
    throw "Erreur lors de la configuration de l'I2C\n";
  }
  setPWMFreq(this->fd, 50); // Fr�quence � 60 Hz.
  setPWM(this->fd, 1, 0, this->impV); // Commande du servo 1 Position Vertical
  setPWM(this->fd, 2, 0, this->impH); // Commande du servo 2 Position Horiozontal
  delay(50);
  }
}

void moveCamera::updateWithJSON(string s)
{
  int dir = stoi(s);
  switch (dir)
  {
    case 'o':
 		    cout_mutex.lock();
        cout << "up" << endl;
    		cout_mutex.unlock();
        while (this->angleV < ANG_V_CEN && GetcurrentInput() == dir)
        {
          this->angleV++;
          angleToImp();
          execute();
        }
        break;
    case 'l':
 		    cout_mutex.lock();
        cout << "down" << endl;
    		cout_mutex.unlock();
        while (this->angleV > ANG_V_BAS && GetcurrentInput() == dir)
        {
          this->angleV--;
          angleToImp();
          execute();
        }
        break;
    case 'k':
 		    cout_mutex.lock();
        cout << "left" << endl;
    		cout_mutex.unlock();
        while (this->angleH > ANG_H_GAU && GetcurrentInput() == dir)
        {
          this->angleH--;
          angleToImp();
          execute();
        }
        break;
    case 'm':
 		    cout_mutex.lock();
        cout << "right" << endl;
    		cout_mutex.unlock();
        while (this->angleH < ANG_H_DRO && GetcurrentInput() == dir)
        {
          this->angleH++;
          angleToImp();
          execute();
        }
        break;
    case 'i':
    case 'x':
 		    cout_mutex.lock();
        cout << "center" << endl;
    		cout_mutex.unlock();
        SetangleV(90);
        SetangleH(0);
        angleToImp();
        execute();
        break;
    default:
 		    cout_mutex.lock();
        cout << "stop" << endl;
    		cout_mutex.unlock();
      	break;

  }
  logRecord();

}

void moveCamera::logRecord()
{
    // Ouverture fichier log
    ofstream fichierLog("cmdRover.log", ios_base::app);
    if (!fichierLog) throw "echec ouverture fichier log";

    // Ecriture date et heure
    time_t dh = time(NULL);
    struct tm dateHeure = *localtime(&dh);
    fichierLog << dateHeure.tm_year + 1900 << "/" << dateHeure.tm_mon + 1 << "/" << dateHeure.tm_mday << "-";
    fichierLog << dateHeure.tm_hour << "h" << dateHeure.tm_min << "m" << dateHeure.tm_sec << "s- Camera: ";

    // Ecriture de l'input
    fichierLog << angleV << "� Vertical - " << angleH << "� Horizontal" << endl;

    // Fermeture fichier
    fichierLog.close();
}

//! \brief transmission de l input du terminal au rover
void moveCamera::update(int key)
{
  SetcurrentInput(key);
  sendJSON();
}
