#ifndef SERVER_H
#define SERVER_H

// Bibliotheque maison
#include "pilotage.h"

// Definitions
#define MAX 1024 //taille maximale des messages a envoyer
#define SA struct sockaddr // raccourci pour la structure "sockaddr"
#define CONCURRENT_CONNECTION 10 // maximum concurrent connections
#define QUEUE_CONNECTION 20 // maximum connection requests queued
#define THREAD_STACK_SIZE 65536 // Thread stack size 64KB

// Classe socket version serveur
class Server:
	  public Pilotage
{
private:
	int port; // port d'ecoute du serveur
	int sockfd; // descripteur de fichier du socket
  // int connfd[CONCURRENT_CONNECTION];
  // int connection; // total connections count (= cle de la map)
  map<int, thread> activeThreads; // vecteur de thread
  // pthread_attr_t attr; // thread attribute
  //static void * InternalThreadFunc(void * This) {((SocketServer *)This)->chat(); return NULL;}
  map<string, int> mapCode = {
    {"capcam", 1},
    {"mvCam", 2},
    {"mvRov", 3},
    {"ODM_gr5", 4},
    {"ODM", 5}
  };
  moveCamera *cam;
  moveRover *rov;
  

protected:
  void chat(int);
  
public:
	// Constructeur
	Server(int);

	// Destructeur
	~Server();

	// Setters / Getters
	int getPort() const { return port; };
  
	// Methode d'ecoute
	void ecoute();

	// Methode de chat

	// Methode de decodage jSon
	map<string, string> decode(const char*);
};

#endif // SERVER_H
