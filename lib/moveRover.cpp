#include "moveRover.h"

moveRover::moveRover()
    :Donnee()
{
    Setcode("mvRov");
    SetcurrentInput('f');
    SetpreviousInput('f');
    SetpreviousTime(time(NULL));
    SetposX(0);
    SetposY(0);
    Setdir(0);
    Setdist(0);
    SettransSpeed(HIGH_SPEED);
    SetrotSpeed(HIGH_SPEED);

    cout_mutex.lock();
    cout << "Construction de moveRover " << endl;
    cout_mutex.unlock();
}

moveRover::~moveRover()
{
    cout_mutex.lock();
    cout << "Destruction de moveRover" << endl;
    cout_mutex.unlock();
}

string moveRover::maDonneeToJSON()
{
    char input[2];
    input[0] = (char)GetcurrentInput();
    //gcvt(this->currentInput, 4, input);

    // Declaration de l'objet Json
    cJSON *root;
    /* Inititaliastion: create root node and array */
    root = cJSON_CreateObject();
    // Affectation des donn�es au Json
    cJSON_AddItemToObject(root, "Code", cJSON_CreateString(Getcode().c_str()));
    cJSON_AddItemToObject(root, "Input", cJSON_CreateString(input));

    // Mise en forme des donn�es au format Json et copie dans un string
    string ordres="";
    // cout << this->input << endl;
    ostringstream oss;
    oss << cJSON_Print(root);
    ordres = oss.str();
    return ordres;
}


//! \brief etablissement de la connexion tx-rx et envoi de commande a l arduino
void moveRover::execute()
{
  int fd;
  if(wiringPiSetup() < 0) throw "Impossible de configurer le serial";
  if((fd = serialOpen("/dev/ttyAMA0",57600)) < 0) throw "Impossible de connecter l'arduino";

  cout_mutex.lock();
  cout << "Envoi a Arduino" << endl;
  cout_mutex.unlock();
  serialPutchar(fd, (char)GetcurrentInput());

  serialClose(fd);
}

void moveRover::updateWithJSON(string s)
{
  SetpreviousInput(GetcurrentInput());
  SetcurrentInput(s[0]);
  logRecord();
  execute();
}

void moveRover::logRecord()
{
    // Ouverture fichier log
    ofstream fichierLog("cmdRover.log", ios_base::app);
    if (!fichierLog) throw "echec ouverture fichier log";

    // Calcul du temps pass�, de la translation et de la rotation
    time_t dh = time(NULL);
    int dt = (intmax_t)dh - (intmax_t)previousTime;
    SetpreviousTime(dh);
    int input = GetpreviousInput();
    float alpha = 0;
    switch (input)
    {
      case 'z':
        Setdist(dt * GettransSpeed());
        SetposX(GetposX() + Getdist()*cos(Getdir()*(2*3.14)/360));
        SetposY(GetposY() + Getdist()*sin(Getdir()*(2*3.14)/360));
        Setdist(0);
        break;
      case 's':
        Setdist(dt * GettransSpeed());
        SetposX(GetposX() - Getdist()*cos(Getdir()*(2*3.14)/360));
        SetposY(GetposY() - Getdist()*sin(Getdir()*(2*3.14)/360));
        Setdist(0);
        break;
      case 'q':
        Setdir(Getdir() - dt * GetrotSpeed());
        break;
      case 'd':
        Setdir(Getdir() + dt * GetrotSpeed());
        break;
      case 'a':
        alpha = (GettransSpeed() * 360) / (L_TANK * dt * 3.14);
        Setdist(3 * L_TANK * tan(alpha) / 2);
        Setdir(Getdir() - (3.14 - alpha)*360/(2*3.14));
        SetposX(GetposX() + Getdist()*cos(Getdir()*(2*3.14)/360));
        SetposY(GetposY() + Getdist()*sin(Getdir()*(2*3.14)/360));
        break;
      case 'e':
        alpha = (GettransSpeed() * 360) / (L_TANK * dt * 3.14);
        Setdist(3 * L_TANK * tan(alpha) / 2);
        Setdir(Getdir() + (3.14 - alpha)*360/(2*3.14));
        SetposX(GetposX() + Getdist()*cos(Getdir()*(2*3.14)/360));
        SetposY(GetposY() + Getdist()*sin(Getdir()*(2*3.14)/360));
        break;
      case 'w':
        alpha = (GettransSpeed() * 360) / (L_TANK * dt * 3.14);
        Setdist(3 * L_TANK * tan(alpha) / 2);
        Setdir(Getdir() + (3.14 - alpha)*360/(2*3.14));
        SetposX(GetposX() - Getdist()*cos(Getdir()*(2*3.14)/360));
        SetposY(GetposY() - Getdist()*sin(Getdir()*(2*3.14)/360));
        break;
      case 'c':
        alpha = (GettransSpeed() * 360) / (L_TANK * dt * 3.14);
        Setdist(3 * L_TANK * tan(alpha) / 2);
        Setdir(Getdir() - (3.14 - alpha)*360/(2*3.14));
        SetposX(GetposX() - Getdist()*cos(Getdir()*(2*3.14)/360));
        SetposY(GetposY() - Getdist()*sin(Getdir()*(2*3.14)/360));
        break;
      case 'r':
        if (GetrotSpeed() == HIGH_ROT) {SetrotSpeed(LOW_ROT);} else {SetrotSpeed(HIGH_ROT);}
        if (GettransSpeed() == HIGH_ROT) {SettransSpeed(LOW_ROT);} else {SettransSpeed(HIGH_ROT);}
        break;
      default:
        break;
    }


    // Ecriture date et heure
    struct tm dateHeure = *localtime(&dh);
    fichierLog << dateHeure.tm_year + 1900 << "/" << dateHeure.tm_mon + 1 << "/" << dateHeure.tm_mday << "-";
    fichierLog << dateHeure.tm_hour << "h" << dateHeure.tm_min << "m" << dateHeure.tm_sec << "s- Rover: ";

    // Ecriture de l'input
    fichierLog << currentInput << " @(X,Y) = (" << GetposX() << "," << GetposY() << ")" << endl;

    // Fermeture fichier
    fichierLog.close();
}

//! \brief transmission de l input du terminal au rover
void moveRover::update(int key)
{
  SetcurrentInput(key);
  sendJSON();
}
