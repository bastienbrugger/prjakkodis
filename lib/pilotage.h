#ifndef PILOTAGE_H
#define PILOTAGE_H

// Bibliotheque maison
#include "application.h"
#include "moveRover.h"
#include "moveCamera.h"
#include "capturecamera.h"
#include "rapportMission.h"

// Espaces de noms
using namespace std;

class Pilotage : public Application
{
public:
    //! \brief Attribut de classe contenant le message recu du poste de commandement
    static string ordreMission;
    //! \brief Attribut de classe contenant les ODM complets recu du poste de commandement
    static RapportMission *odmGr5, *odmGr7;
};

#endif // PILOTAGE_H
