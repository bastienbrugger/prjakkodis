#ifndef CLIENT_H
#define CLIENT_H

#include "application.h"

#define MAX 1024
// #define PORT 8882
// #define IP_ADR "193.253.53.72"
#define SA struct sockaddr

class Client: public Application
{
    public:
        Client(int=8882, string="193.253.53.72");
        virtual ~Client();

        void setPort(int val) {this->PORT = val;}
        int getPort() const {return this->PORT;}
        void setIP(string val) {this->IP_ADR = val;}
        string getIP() const {return this->IP_ADR;}
        void setsockfd(int val) {this->sockfd = val;}
        int getsockfd() const {return this->sockfd;}

        // Fonction de connexion qui renvoie un int représentant la socket
        int connect_bot();
        // Fonction d'envoi des commandes au serveur
        void order_to_bot(string);

    protected:

    private:
        int PORT;
        string IP_ADR;
        int sockfd;
};

#endif // CLIENT_H
