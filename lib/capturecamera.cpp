#include "capturecamera.h"


CaptureCamera::CaptureCamera()
    :Donnee()
{
    Setcode("capcam");
    cout_mutex.lock();
    cout << "Construction de CaptureCamera" << endl;
    cout_mutex.unlock();
}

CaptureCamera::~CaptureCamera()
{
    cout_mutex.lock();
    cout << "Destruction de CaptureCamera" << endl;
    cout_mutex.unlock();
}

string CaptureCamera::maDonneeToJSON()
{
    // Declaration de l'objet Json
    cJSON *root;
    /* Inititaliastion: create root node and array */
    root = cJSON_CreateObject();
    // Affectation des donnees au Json
    cJSON_AddItemToObject(root, "Code", cJSON_CreateString(Getcode().c_str()));

    string ordres="";

    // Mise en forme des donnees au format Json et copie dans un string
    ostringstream oss;
    oss << cJSON_Print(root);
    ordres = oss.str();
    return ordres;
}

void CaptureCamera::logRecord()
{
    // Ouverture fichier log
    ofstream fichierLog("cmdRover.log", ios_base::app);
    if (!fichierLog) throw "echec ouverture fichier log";

    // Ecriture date et heure
    time_t dh = time(NULL);
    struct tm dateHeure = *localtime(&dh);
    fichierLog << dateHeure.tm_year + 1900 << "/" << dateHeure.tm_mon + 1 << "/" << dateHeure.tm_mday << "-";
    fichierLog << dateHeure.tm_hour << "h" << dateHeure.tm_min << "m" << dateHeure.tm_sec << "s: ";

    // Ecriture donnee au format JSON
    fichierLog << "Prise de vue" << endl;

    // Fermeture fichier
    fichierLog.close();
}

//! \brief Prise de vue par le Rover:
//! \brief - Arret du flux video
//! \brief - Prise de vue
//! \brief - Relance du flux video
//! \brief - Envoi ssh de la photo aux postes de commandement
void CaptureCamera::execute()
{
    // Creation repertoire photos
    system("mkdir -p photos");

    // Arret service streaming
    system("sudo systemctl stop client-video.service");

    // Prise photo
    system("libcamera-still -n -o photos/gr2_mine.jpg");
    logRecord();

    // Redemarrage service streaming
    system("sudo systemctl start client-video.service");

    // Envoi photo au gr5
    system("sudo ssh -i key/cle_photo bastien@57.128.110.200 \"mkdir -p /home/bastien/prjakkodis/photos\"");
    system("sudo scp -i key/cle_photo photos/gr2_mine.jpg bastien@57.128.110.200:/home/bastien/prjakkodis/photos/gr2_mine.jpg");
    // Envoi photo au gr7
    system("sudo ssh -i key/cle_photo francois@57.128.110.200 \"mkdir -p /home/francois/prjakkodis/photos\"");
    system("sudo scp -i key/cle_photo photos/gr2_mine.jpg francois@57.128.110.200:/home/francois/prjakkodis/photos/gr2_mine.jpg");
}

void CaptureCamera::updateWithJSON(string s)
{
}
