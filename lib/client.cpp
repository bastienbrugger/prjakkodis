#include "client.h"

Client::Client(int p, string ip)
{
    this->PORT=p;
    this->IP_ADR=ip;
    //Connexion
    this->sockfd = connect_bot();
    if (this->sockfd == 1)
    {
        throw "Erreur socket";
    }
    cout_mutex.lock();
    cout << "construction du client" << endl;
    cout_mutex.unlock();
}

Client::~Client()
{
    // close the socket
    close(this->sockfd);
    cout_mutex.lock();
    cout << "Destruction du Client" << endl;
    cout_mutex.unlock();
}

//! \fn int Client::connect_bot()
//! \brief etablissement de la connexion
int Client::connect_bot()
{
    int sock;
        struct sockaddr_in servaddr;

        // Cr�ation de socket
        sock = socket(AF_INET, SOCK_STREAM, 0);
        if (sock == -1) {
                throw "[-] Echec creation socket";
        }
        else
				{
    		 		cout_mutex.lock();
				 		cout << "[+] Succes creation socket" << endl;
			      cout_mutex.unlock();
			  }
        bzero(&servaddr, sizeof(servaddr));

        // Affectation PORT, IP
        servaddr.sin_family = AF_INET;
        servaddr.sin_addr.s_addr = inet_addr(this->IP_ADR.c_str());
        servaddr.sin_port = htons(this->PORT);

        // connexion des sockets client et serveur
        if (connect(sock, (SA*)&servaddr, sizeof(servaddr))!= 0)
    {
                throw "[-] Echec connexion avec le serveur";
        }
        else
        {
    		 				cout_mutex.lock();
                cout << "[+] Succes connexion avec le serveur" << endl;
							 	cout_mutex.unlock();
        }
    return sock;
}

//! \fn void Client::order_to_bot(string ordres)
//! \brief envoi d un string passe en argument au serveur distant
void Client::order_to_bot(string ordres)
{

    // Ecriture du string dans le socket
    write(this->sockfd, ordres.c_str(), ordres.length());

    char buff[MAX];
    read(this->sockfd, buff, sizeof(buff));
    cout_mutex.lock();
    cout << "From Server : " << buff << endl;
    cout_mutex.unlock();

}

