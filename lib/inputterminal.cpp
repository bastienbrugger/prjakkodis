#include "inputterminal.h"


//! \brief Constructeur du terminal de commande
//! \brief - Creation d un ecran nCurses
//! \brief - Affichage des commandes disponible
inputTerminal::inputTerminal()
{
    initscr();
    noecho();
    halfdelay(6);

    Setinput(-1);
    SetinputOld(-1);
    this->mvCam = new moveCamera();
    this->mvRov = new moveRover();

    this->oss.str("");

    mvprintw(0, 0, "(En attente d'ordres...)\n");
    mvprintw(17, 0, "\n");

    mvprintw(3, 0, "COMMANDES :");
    mvprintw(4, 5, "zqsd & aewc : bouger le rover");
    mvprintw(5, 5, "f : stopper le rover");
    mvprintw(6, 5, "r : basculer en mode rapide ou lent");
    mvprintw(7, 5, "oklm : pivoter la camera");
    mvprintw(8, 5, "i : centrer la camera");
    mvprintw(9, 5, "j : stopper la camera");
    mvprintw(10, 5, "p : prendre une photo");
    mvprintw(12, 5, "t : envoyer rapport etat de mission");
    mvprintw(13, 5, "y : envoyer rapport mine trouvee");
    mvprintw(14, 5, "n : envoyer rapport anomalie");

    mvprintw(16, 5, "x : quitter");

    mvprintw(18, 0, "ORDRES DE MISSION :");

    mvprintw(20,0, "");
    mvprintw(21,0, "");
    mvprintw(22,0, "");
    mvprintw(23,0, "");
    mvprintw(24,0, "");
    mvprintw(25,0, "");
}

inputTerminal::~inputTerminal()
{
    endwin();

    if (mvCam!=nullptr)
    {
        delete mvCam;
        mvCam=nullptr;
    }
    if (mvRov!=nullptr)
    {
        delete mvRov;
        mvRov=nullptr;
    }
}

//! \brief Terminal Principal
//! \brief - Ecoute des inputs du clavier
//! \brief - Envoi des ordres au rover et au poste de commandement
//! \brief - Affichage des messages recus et des sorties standarts du programme
//! \brief - Affichage de la derniere photo prise si disponible
void inputTerminal::launchIHM()
{
 	  int lineL = 0;
 	  int lineR = 0;

    bool stop = false;

    while (!stop)
    {
        input = getch();

        if (input != inputOld)
        {
            switch (input)
            {
            // Deplacement rover
            case 'z':
                mvprintw(0, 0, "Avance\n");
                mvprintw(17, 0, "\n");
                mvRov->update(input);
                break;

            case 's':
                mvprintw(0, 0, "Recule\n");
                mvprintw(17, 0, "\n");
                mvRov->update(input);
                break;

            case 'q':
                mvprintw(0, 0, "Pivote a gauche\n");
                mvprintw(17, 0, "\n");
                mvRov->update(input);
                break;

            case 'd':
                mvprintw(0, 0, "Pivote a droite\n");
                mvprintw(17, 0, "\n");
                mvRov->update(input);
                break;

            case 'a':
                mvprintw(0, 0, "Avance a gauche\n");
                mvprintw(17, 0, "\n");
                mvRov->update(input);
                break;

            case 'e':
                mvprintw(0, 0, "Avance a droite\n");
                mvprintw(17, 0, "\n");
                mvRov->update(input);
                break;

            case 'w':
                mvprintw(0, 0, "Recule a gauche\n");
                mvprintw(17, 0, "\n");
                mvRov->update(input);
                break;

            case 'c':
                mvprintw(0, 0, "Recule a droite\n");
                mvprintw(17, 0, "\n");
                mvRov->update(input);
                break;

            case 'f':
                mvprintw(0, 0, "Stoppe le rover\n");
                mvprintw(17, 0, "\n");
                mvRov->update(input);
                break;

            case 'r':
                mvprintw(0, 0, "Change de vitesse\n");
                mvprintw(17, 0, "\n");
                mvRov->update(input);
                break;

            // Deplacement camera
            case 'o':
                mvprintw(0, 0, "Monte la camera\n");
                mvprintw(17, 0, "\n");
                mvCam->update(input);
                break;

            case 'l':
                mvprintw(0, 0, "Baisse la camera\n");
                mvprintw(17, 0, "\n");
                mvCam->update(input);
                break;

            case 'k':
                mvprintw(0, 0, "Pivote la camera a gauche\n");
                mvprintw(17, 0, "\n");
                mvCam->update(input);
                break;

            case 'm':
                mvprintw(0, 0, "Pivote la camera a droite\n");
                mvprintw(17, 0, "\n");
                mvCam->update(input);
                break;

            case 'i':
                mvprintw(0, 0, "Centre la camera\n");
                mvprintw(17, 0, "\n");
                mvCam->update(input);
                break;

            case 'j':
                mvprintw(0, 0, "Stoppe la camera\n");
                mvprintw(17, 0, "\n");
                mvCam->update(input);
                break;

            // Prise photo
            case 'p':
            {
                mvprintw(0, 0, "Prends une photo\n");
                mvprintw(17, 0, "\n");
                mvRov->update('f');
                mvCam->update('j');
                CaptureCamera* capCam = new CaptureCamera();
                system("mkdir -p photos");
                system("rm -rf photos/gr2_mine.jpg");
                capCam->sendJSON();
                bool q = false;
                do
                {
                  ifstream imageRaw("photos/gr2_mine.jpg", ios_base::in);
                  if (imageRaw) { q = true; imageRaw.close(); }
                  sleep(1);
                } while (!q);
                system("img2txt -W 100 -f ansi photos/gr2_mine.jpg > photos/output");
                this->afficherPhoto();

                if (odmGr5 != nullptr)
                {
                  odmGr5->updateWithJSON("112");
                }
                if (odmGr7 != nullptr)
                {
                  odmGr7->updateWithJSON("112");
                }
                if (capCam!=nullptr)
                {
                    delete capCam;
                    capCam=nullptr;
                }
                break;
            }
            // Gestion du RDM
            case 't':
              mvprintw(0, 0, "Envoi Rapport\n");
              mvprintw(17, 0, "\n");
              if (odmGr5 != nullptr)
              {
                odmGr5->updateWithJSON("116");
                if (odmGr5->Getetat() == "End Mission") odmGr5 = nullptr;
              }
              if (odmGr7 != nullptr)
              {
                odmGr7->updateWithJSON("116");
                if (odmGr7->Getetat() == "End Mission") odmGr7 = nullptr;
              }
              break;
            case 'y':
              mvprintw(0, 0, "Detection Mine\n");
              mvprintw(17, 0, "\n");
              if (odmGr5 != nullptr)
              {
                odmGr5->updateWithJSON("121");
              }
              if (odmGr7 != nullptr)
              {
                odmGr7->updateWithJSON("121");
              }
              break;
            case 'n':
              mvprintw(0, 0, "Anomalie\n");
              mvprintw(17, 0, "\n");
              if (odmGr5 != nullptr)
              {
                odmGr5->updateWithJSON("110");
              }
              if (odmGr7 != nullptr)
              {
                odmGr7->updateWithJSON("110");
              }
              break;

            // Arret d'appui sur une touche
            case -1:
                // Apres deplacement rover
                if (inputOld == 'z' || inputOld == 'q' || inputOld == 's' || inputOld == 'd' || inputOld == 'a' || inputOld == 'e' || inputOld == 'w' || inputOld == 'c')
                {
                    mvprintw(0, 0, "Arrete de bouger\n");
                    mvprintw(17, 0, "\n");
                    mvRov->update('f');
                }
                // Apres deplacement camera
                if (inputOld == 'o' || inputOld == 'k' || inputOld == 'l' || inputOld == 'm')
                {
                    mvprintw(0, 0, "Arrete de pivoter la camera\n");
                    mvprintw(17, 0, "\n");
                    mvCam->update('j');
                }
                break;

            // Quitter
            case 'x':
                stop = true;
                mvRov->update('x');
                mvCam->update('x');
                break;

            // Autres touches
            default:
                mvprintw(0, 0, "(%c non reconnu)\n", input);
                mvprintw(17, 0, "\n");
                break;
            }

            inputOld = input;

        }

        // Affichage de l'ordre du poste de commandement
        if (!ordreMission.empty()) {
		        while (ordreMission.length() > 50)
		        {
		            mvprintw(20+lineL,0, ordreMission.substr(0,49).c_str());
		            ordreMission.erase(0,49);
		            lineL++;
		            if (lineL > 5) lineL=0;
		        }
		        mvprintw(20+lineL,0, ordreMission.substr(0,ordreMission.length()).c_str());
		        for (int i=0; i<(49-ordreMission.length()); i++)
		        {
		         		mvprintw(20+lineL,ordreMission.length()+i, " ");
		        }
          	lineL++;
          	if (lineL > 5) lineL=0;
		        ordreMission.erase(0,ordreMission.length());
        }

        // Decharge du COUT

    		cout_mutex.lock();
        string coutMessage = this->oss.str();
        this->oss.str("");
    		cout_mutex.unlock();
				mvprintw(46,0, coutMessage.c_str());
    		/*
        while (!coutMessage.empty())
				{
		        string delimiter = "\n";
		        string token;

		        int pos = 0;
  					pos = coutMessage.find(delimiter);

		        if (pos != std::string::npos)
		        {
		            token = coutMessage.substr(0, pos);

		            while (token.length() > 50)
		            {
		                mvprintw(30+lineR,0, token.substr(0,49).c_str());
		                token.erase(0,49);
		                lineR++;
		                if (lineR > 5) lineR=0;
		            }
		            mvprintw(30+lineR,0, token.substr(0,token.length()).c_str());
		            token = "";
		            coutMessage.erase(0, pos + delimiter.length());
		        }
	          else
	          {
	           		token = coutMessage;

		            while (token.length() > 50)
		            {
		                mvprintw(30+lineR,0, token.substr(0,49).c_str());
		                token.erase(0,49);
		                lineR++;
		                if (lineR > 5) lineR=0;
		            }
		            mvprintw(30+lineR,0, token.substr(0,token.length()).c_str());
		            token = "";
		            coutMessage = "";
	          }
        }
        */
    }
}

//! \brief Parametrage des couleurs
//! \param prend un code couleur en argument de 0 � 7
//! \return renvoi la constante de couleur nCurses correspodnante
short inputTerminal::curs_color(int fg)
{
    switch (fg) {           /* RGB */
    case 0:                     /* 000 */
        return (COLOR_BLACK);
    case 1:                     /* 001 */
        return (COLOR_BLUE);
    case 2:                     /* 010 */
        return (COLOR_GREEN);
    case 3:                     /* 011 */
        return (COLOR_CYAN);
    case 4:                     /* 100 */
        return (COLOR_RED);
    case 5:                     /* 101 */
        return (COLOR_MAGENTA);
    case 6:                     /* 110 */
        return (COLOR_YELLOW);
    case 7:                     /* 111 */
        return (COLOR_WHITE);
    }
}

//! \brief Outils de correspondance entre les codes ANSI et les couleurs nCurses
short inputTerminal::index_find(int f, int b)
{
  return 8*f + b + 1;
}

//! \brief creation de toutes les associstaions de couleurs possible
void inputTerminal::pair_init()
{
  short cpt = 1;
  for (int i=0; i<8;i++)
  {
    for (int j=0; j<8; j++)
    {
      init_pair(cpt, curs_color(i), curs_color(j));
      cpt++;
    }
  }

}

//! \brief affichage de la photo au format ansi
void inputTerminal::afficherPhoto()
{
  int lineNb = 0;
  int charNb = 0;
  string lineRead="";
  int foreground;
  int background;
  string token;
  char start = 27;
  char cut = ';';
  char end = 'm';

  start_color();
  pair_init();
  ifstream imageAnsi("photos/output", ios_base::in);
  while(getline(imageAnsi, lineRead))
  {
    while(!lineRead.empty())
    {

      // lecture ligne a ligne de l'image
      token = lineRead.substr(0, lineRead.find(end));
      if (token.length() > 4)
      {
        lineRead.erase(0,2);
        lineRead.erase(0, lineRead.find(end)-5);

        try { foreground = stoi(lineRead.substr(0, lineRead.find(cut))) - 30; lineRead.erase(0,3);}
        catch (...) { foreground = 0;}

        if (lineRead.find(end) > 1)
        {
          try {background = stoi(lineRead.substr(0, lineRead.find(end))) - 40;lineRead.erase(0,3);}
          catch (...) { background = 0;}
        } else background = 0;

        token = lineRead.substr(0, lineRead.find(start));
        lineRead.erase(0,token.length());

        // ecriture caractere par caractere de l'image
        for (int i=0; i<token.length();i++)
        {
          attron(COLOR_PAIR(index_find(foreground, background)));
          mvaddch(lineNb, charNb + 75, token[i]);
          attroff(COLOR_PAIR(index_find(foreground, background)));
          charNb++;
        }
      }
      else
      {
        lineRead = "";
      }
    }
    lineNb++;
    charNb=0;
  }
}


