#ifndef INPUTTERMINAL_H
#define INPUTTERMINAL_H

// Bibliotheques donnees
#include "pilotage.h"

// Espaces de noms
using namespace std;

class inputTerminal:
	  public Pilotage
{
  private:
    int input = -1;
    int inputOld = -1;
    moveCamera* mvCam;
    moveRover* mvRov;
    
    
  public:
        inputTerminal();
        ~inputTerminal();
        
        ostringstream oss;
        
        void Setinput(int val) {input = val;}
        int Getinput() {return input;}
        void SetinputOld(int val) {inputOld = val;}
        int GetinputOld() {return inputOld;}
        
        void launchIHM();
        short curs_color(int);
        short index_find(int, int);
        void pair_init();
        void afficherPhoto();   
};

#endif // INTPUTTERMINAL_H
