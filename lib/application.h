#ifndef APPLICATION_H
#define APPLICATION_H

// Bibliotheques de base
#include <iostream>
#include <fstream>
#include <sstream>
#include <ctime>
#include <cstring>
#include <map>
#include <signal.h>
#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <wiringSerial.h>
#include <string>
#include <strings.h> // bzero() -- initialise � 0 le bloc m�moire en param1, param2 la taille � mettre � 0
#include <ctype.h>

// Doublons
#include <cstdio>
#include <cstdlib>

// Bibliotheques reseau
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <arpa/inet.h> // inet_addr()

// Bibliotheques pthread
#include <thread>
#include <mutex>
#include <vector>

// Bilbiotheque JSON
#include "cJSON.h"

// Espaces de noms
using namespace std;

class Application
{
public:
    //! \brief Mutex pour ordonnancer les sorties standarts des differentes taches
    //! \version 1.0
    //! \date 28 juillet 2023
    //!
    mutex cout_mutex;
};

#endif // APPLICATION_H
