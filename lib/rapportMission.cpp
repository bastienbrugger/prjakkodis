#include "rapportMission.h"

RapportMission::RapportMission(string c, string i, string z)
    :Donnee()
{
    Setcode("RDM_gr2");
    Setcommandement(c);
    SetId(i);
    Setetat("newODM");
    Sethorodatage();
    Setzone(z);
    Setmessage("No message");
    cout_mutex.lock();
    cout << "Construction de RapportMission" << endl;
    cout_mutex.unlock();
}

RapportMission::~RapportMission()
{
    cout_mutex.lock();
    cout << "Destruction de RapportMission" << endl;
    cout_mutex.unlock();
}

string RapportMission::maDonneeToJSON()
{
    // Declaration de l'objet Json
    cJSON *root;
    /* Inititaliastion: create root node and array */
    root = cJSON_CreateObject();
    // Affectation des donnees au Json
    if (Getcommandement() == "GR7")
    {
      Setcode("RDM");
      cJSON_AddItemToObject(root, "Code", cJSON_CreateString(Getcode().c_str()));
      cJSON_AddItemToObject(root, "Expediteur", cJSON_CreateString("groupe_2"));
      cJSON_AddItemToObject(root, "Id", cJSON_CreateString(GetId().c_str()));
      cJSON_AddItemToObject(root, "Etat", cJSON_CreateString(Getetat().c_str()));
      cJSON_AddItemToObject(root, "Horodatage", cJSON_CreateString(Gethorodatage().c_str()));
      cJSON_AddItemToObject(root, "Zone", cJSON_CreateString(Getzone().c_str()));
      cJSON_AddItemToObject(root, "Message", cJSON_CreateString(Getmessage().c_str()));
    }
    if (Getcommandement() == "GR5")
    {
      cJSON_AddItemToObject(root, "Code", cJSON_CreateString(Getcode().c_str()));
      cJSON_AddItemToObject(root, "id_mission", cJSON_CreateString(GetId().c_str()));
      cJSON_AddItemToObject(root, "objet", cJSON_CreateString("Test"));
      cJSON_AddItemToObject(root, "rover", cJSON_CreateString("Groupe 2"));
      cJSON_AddItemToObject(root, "type", cJSON_CreateString("observation"));
      cJSON_AddItemToObject(root, "etat", cJSON_CreateString(Getetat().c_str()));
      cJSON_AddItemToObject(root, "horodatage", cJSON_CreateString(Gethorodatage().c_str()));
      cJSON_AddItemToObject(root, "lieu", cJSON_CreateString(Getzone().c_str()));
      cJSON_AddItemToObject(root, "action", cJSON_CreateString(Getmessage().c_str()));
    }


    // cJSON_AddItemToObject(root, "Commandement", cJSON_CreateString(Getcommandement().c_str()));


    string ordres="";
    // Mise en forme des donnees au format Json et copie dans un string
    ostringstream oss;
    if (Getcommandement() == "GR7") oss << cJSON_PrintUnformatted(root);
    if (Getcommandement() == "GR5") oss << cJSON_Print(root);
    ordres = oss.str();
    return ordres;
}

void RapportMission::logRecord()
{
    // Ouverture fichier log
    ofstream fichierLog("cmdRover.log", ios_base::app);
    if (!fichierLog) throw "echec ouverture fichier log";

    // Ecriture date et heure
    time_t dh = time(NULL);
    struct tm dateHeure = *localtime(&dh);
    fichierLog << dateHeure.tm_year + 1900 << "/" << dateHeure.tm_mon + 1 << "/" << dateHeure.tm_mday << "-";
    fichierLog << dateHeure.tm_hour << "h" << dateHeure.tm_min << "m" << dateHeure.tm_sec << "s: ";

    // Ecriture donnee au format JSON
    fichierLog << "Envoi Rapport" << endl;

    // Fermeture fichier
    fichierLog.close();
}

//! \brief envoi du rapport de mission au poste de commandement correspondant a l objet actif
void RapportMission::execute()
{
    this->maDonneeToJSON();
    if (Getcommandement() == "GR5")
    {
      this->sendJSON(8000,"57.128.108.126");
      // this->sendJSON();
    }
    else if (Getcommandement() == "GR7")
    {
      this->sendJSON(8080,"57.128.107.86");
      // this->sendJSON();
    }
    else
    {
     		cout_mutex.lock();
    		cout << "erreur poste de commandement non valide" << endl;
    		cout_mutex.unlock();
    }

    logRecord();
}

void RapportMission::updateWithJSON(string s)
{
  int choix = stoi(s);
  Sethorodatage();
  switch (choix)
  {
    case 't':
      if (Getcommandement() == "GR7")
      {
        if (Getetat() == "newODM")
				{
				 	  Setetat("En cours");
			 	    Setmessage("Debut mission");
				}
				else
				{
				 		Setetat("Reussite");
			 	    Setmessage("Fin mission, reussite");
				}
      }
      if (Getcommandement() == "GR5")
      {
        if (Getetat() == "newODM") Setetat("Start Mission"); else Setetat("End Mission");
      }
      break;
    case 'y':
      if (Getcommandement() == "GR7")
      {
        Setetat("Reussite");
        Setmessage("Mine trouvee");
      }
      if (Getcommandement() == "GR5")
      {
        Setetat("Mine Trouve");
      }
      break;
    case 'n':
      if (Getcommandement() == "GR7")
      {
        Setetat("Echec");
        Setmessage("Echec mission, anomalie");
      }
      if (Getcommandement() == "GR5")
      {
        Setetat("Anomalie");
      }
      break;
    case 'p':
      if (Getcommandement() == "GR7")
      {
        Setmessage("Prise de Vue");
      }
      if (Getcommandement() == "GR5")
      {
        Setmessage("Prise de Vue");
      }
      break;
    default:
      break;
  }
  // Modif pour la demo : on n'envoie rien au groupe 5
  if (Getcommandement() == "GR7")
  {
   	 execute();
  }
}

string RapportMission::Gethorodatage()
{
  struct tm dateHeure = *localtime(&this->horodatage);
  ostringstream oss;
  oss << dateHeure.tm_year + 1900 << "/" << dateHeure.tm_mon + 1 << "/" << dateHeure.tm_mday << "-";
  oss << dateHeure.tm_hour << "h" << dateHeure.tm_min << "m" << dateHeure.tm_sec << "s";
  return oss.str();
}
