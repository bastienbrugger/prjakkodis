#ifndef CAPTURECAMERA_H
#define CAPTURECAMERA_H

#include "donnee.h"


class CaptureCamera: public Donnee
{
    public:
        CaptureCamera();
        ~CaptureCamera();

        virtual void updateWithJSON(string) override;
        virtual string maDonneeToJSON() override;
        virtual void logRecord() override;
        virtual void execute() override;
};

#endif // CAPTURECAMERA_H
