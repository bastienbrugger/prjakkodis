#ifndef MOVEROVER_H
#define MOVEROVER_H

#include "donnee.h"


// Vitesse de translation du rover en m/s
#define HIGH_SPEED 0.14
#define LOW_SPEED 0.09

// Vitesse de rotation du rover en �/s
#define HIGH_ROT 60
#define LOW_ROT 30

// Largeur du tank entre les centres des chenilles en m
#define L_TANK 0.2

class moveRover: public Donnee
{
    private:
        int currentInput;
        int previousInput;
        time_t previousTime;
        float dist, dir;
        float posX, posY;
        float transSpeed, rotSpeed;
        
    public:
        moveRover();
        ~moveRover();

        int GetcurrentInput() const { return currentInput; }
        void SetcurrentInput(int val) {currentInput = val; }
        int GetpreviousInput() const { return previousInput; }
        void SetpreviousInput(int val) {previousInput = val; }
        time_t GetpreviousTime() const { return previousTime; }
        void SetpreviousTime(time_t val) {previousTime = val; }
        float Getdist() const { return dist; }
        void Setdist(float val) {dist = val; }
        float Getdir() const { return dir; }
        void Setdir(float val) { if (val < 0) {dir = val + 360;} else if (val >= 360) {dir = val - 360;} else {dir = val;} }
        float GetposX() const { return posX; }
        void SetposX(float val) {posX = val; }
        float GetposY() const { return posY; }
        void SetposY(float val) {posY = val; }
        float GettransSpeed() const { return transSpeed; }
        void SettransSpeed(float val) {transSpeed = val; }
        float GetrotSpeed() const { return rotSpeed; }
        void SetrotSpeed(float val) {rotSpeed = val; }
        
        void update(int key);
        
        virtual string maDonneeToJSON() override;
        virtual void updateWithJSON(string s) override;
        virtual void logRecord() override;
        virtual void execute() override;
};

#endif // MOVEROVER_H
