#ifndef MOVECAMERA_H
#define MOVECAMERA_H

#include "donnee.h"

#define PCA9685_MODE1 0x00
#define PCA9685_PRESCALE 0xFE
#define LED0_ON_L 0x06
#define LED0_ON_H 0x07
#define LED0_OFF_L 0x08
#define LED0_OFF_H 0x09

#define ANG_H_GAU -45
#define ANG_H_DRO 45
// #define ANG_H_CEN 0
#define ANG_V_CEN 90
#define ANG_V_BAS 30

#define IMP_H_GAU 240
#define IMP_H_DRO 440
// #define IMP_H_CEN 340
#define IMP_V_CEN 120
#define IMP_V_BAS 210


// Remplacer par l'adresse I2C de votre carte PCA9685.
#define PCA9685_I2C_ADDR 0x40 


class moveCamera: public Donnee
{
    public:
        moveCamera();
        moveCamera(int, int);
        ~moveCamera();

        int GetangleV() const { return angleV; }
        void SetangleV(int val) { angleV = val; }
        int GetangleH() const { return angleH; }
        void SetangleH(int val) { angleH = val; }
        
        int GetimpV() const { return impV; }
        void SetimpV(int val) { impV = val; }
        int GetimpH() const { return impH; }
        void SetimpH(int val) { impH = val; }
        
        int Getfd() const { return fd; }
        void Setfd(int val) {fd = val; }
        int GetcurrentInput() const { return currentInput; }
        void SetcurrentInput(int val) {currentInput = val; }
        
        
        void angleToImp();
        void setPWMFreq(int fd, int freq);
        void setPWM(int fd, int channel, int on, int off);
        void update(int key);
        

        virtual string maDonneeToJSON() override;
        virtual void updateWithJSON(string s) override;
        virtual void logRecord() override;
        virtual void execute() override;
    protected:

    private:
        int currentInput;
        int angleV, angleH;
        int impV, impH;
        int fd;
        
};

#endif // MOVECAMERA_H
