#include "server.h"

//! \brief deconnexion du serveur en cas de CTRL C
void signalHandler(sig_atomic_t s)
{
  exit(0);
}

//! \brief Constructeur du Serveur
//! \brief - creation du socket et liaison au port
//! \brief - instanciation des objets necessaires
Server::Server(int p)
{
    // Initialisation du port
    port = p;

    // Declarations
    struct sockaddr_in servaddr; // adresse du serveur

    // Creation et verification du socket IPv4 de type TCP
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1) throw "echec creation socket";
    else
		{
	    cout_mutex.lock();
			cout << "Socket cree..." << endl;
	    cout_mutex.unlock();
	 	}

    // Assignation de l'adresse IP et du port
    bzero(&servaddr, sizeof(servaddr)); // vidage contenu de servaddr
    servaddr.sin_family = AF_INET; // protocole IPv4
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY); // ecoute sur toutes les adresses IP
    servaddr.sin_port = htons(getPort()); // attribution du port

    // Liaison du socket a l'adresse IP et verification
    if (bind(sockfd, (SA*)&servaddr, sizeof(servaddr)) != 0) throw "echec liaison socket";
    else
		{
	    cout_mutex.lock();
			cout << "Socket lie au port " << getPort() << "..." << endl;
	    cout_mutex.unlock();
	  }
    this->cam = nullptr;
    this->rov = nullptr;
}

Server::~Server()
{
    // Fermeture du socket
    //cout << "Fermeture du socket..." << endl;
    close(sockfd);
}

//! \brief Captation des connexions entrantes et ordonnacement des taches correspondantes
void Server::ecoute()
{
    // Declaration
    // int connfd; // descripteur de fichier pour la connexion
    struct sockaddr_in cli;
    unsigned int len; // taille de la structure sockaddr_in

    // Boucle infinie sur l'ecoute de connexions et la reception de messages
  // Ouverture du socket aux connections et verification
    if (listen(sockfd, QUEUE_CONNECTION) != 0) throw "echec ecoute";
    else
		{
	    cout_mutex.lock();
			cout << "Serveur ecoute..." << endl;
	    cout_mutex.unlock();
	  }

    while (1)
    {
        // Sortie de boucle si reception d'un signal Ctrl+C
        signal(SIGINT, signalHandler);

        // Acceptation de la demande de connexion et verification
        len = sizeof(cli);
        int cle= accept(sockfd, (SA*)&cli, &len);
        if (cle < 0) throw "echec acceptation serveur";
        else if (activeThreads.size() >= CONCURRENT_CONNECTION)
				{
						cout_mutex.lock();
						cout << "[WARNING] CONNECTION LIMITE REACHED" << endl;
		    		cout_mutex.unlock();
			  }
        else
        {
          cout_mutex.lock();
          cout << "[INFO] NEW CONNECTION ACCEPTED FROM " << inet_ntoa(cli.sin_addr) << ":" << ntohs(cli.sin_port) << endl;
          cout_mutex.unlock();

          activeThreads[cle] = thread(&Server::chat, this, cle);

          if (!activeThreads.empty())
          {
            for(auto itMap=activeThreads.begin(); itMap!=activeThreads.end(); itMap++)
            {
		 				  cout_mutex.lock();
              cout << "Thread : " << itMap->first << " (" << itMap->second.joinable() << ")" << endl;
				 			cout_mutex.unlock();
              if(!itMap->second.joinable())
              {
    					  cout_mutex.lock();
                cout << "Destruction du thread : " << itMap->first << " (" << itMap->second.joinable() << ")... " << endl;
    						cout_mutex.unlock();
                activeThreads.erase(itMap->first);
                itMap=activeThreads.begin();
                if (activeThreads.empty()) break;

              }
            }
          }
        }
    }
}

//! \brief Lecture et decodage du message recu et dispatch vers les methodes appropries
void Server::chat(int pos)
{
    // Declaration
    char buff[MAX]; // Buffer contenant les messages, limite a un nombre de caracteres defini par MAX
    map<string, string> mainMap;
    // Ouverture fichier log
    ofstream fichierLog("instructions.log", ios_base::app);
    if (!fichierLog) throw "echec ouverture fichier log";

    // Vide le buffer
    bzero(buff, MAX);

    // Lecture du message envoye par le client et copie dans le buffer
    read(pos, buff, sizeof(buff));
    // Ecriture dans le log de la date et l'heure puis du contenu du buffer
    time_t dh = time(NULL);
    struct tm dateHeure = *localtime(&dh);
    fichierLog << dateHeure.tm_year + 1900 << "/" << dateHeure.tm_mon + 1 << "/" << dateHeure.tm_mday << "-";
    fichierLog << dateHeure.tm_hour << "h" << dateHeure.tm_min << "m" << dateHeure.tm_sec << "s";
    fichierLog << " (connexion " << pos << ")" << endl;
    fichierLog << buff << endl;
    //Fermeture fichier
    fichierLog.close();
    // Decodage du JSON
    mainMap = decode(buff);
    // Ecriture et envoi d'un accuse de reception
    strcpy(buff, "message recu\n");
    write(pos, buff, sizeof(buff));
    // Vider le buffer
    bzero(buff, MAX);
    // Close
    close(pos);

    // Work in Progress
    cout_mutex.lock();
    cout << "Cle identifie: " << mainMap["Code"] << endl;
    cout_mutex.unlock();
    switch (this->mapCode[mainMap["Code"]])
    {
    case 1: // CaptureCamera
      {
        CaptureCamera* cap = new CaptureCamera();
        this->cam->updateWithJSON("112");
        cap->execute();
        if (cap!=nullptr)
        {
          delete cap;
          cap=nullptr;
        }
        break;
      }
    case 2: // moveCamera
      {
	 	 	  cout_mutex.lock();
        cout << "input: " << (char)stoi(mainMap["Input"]) << endl;
    		cout_mutex.unlock();
        if (this->cam==nullptr)
          {
            this->cam = new moveCamera(0,90);
          }
          else
          {
    			  cout_mutex.lock();
            cout << "mise a jour de la rotation" << endl;
    				cout_mutex.unlock();

            this->cam->SetcurrentInput(stoi(mainMap["Input"]));
            this->cam->updateWithJSON(mainMap["Input"]);
            if (stoi(mainMap["Input"]) == (int)'x' || stoi(mainMap["Input"]) == (int)'X')
            {
              if (this->cam!=nullptr)
              {
                delete this->cam;
                this->cam=nullptr;
              }
            }
          }
        break;
      }
    case 3: // moveRover
      {
    	  cout_mutex.lock();
        cout << "input: " << mainMap["Input"] << endl;
    		cout_mutex.unlock();
        if (this->rov==nullptr)
        {
          this->rov = new moveRover();
        }

    		cout_mutex.lock();
        cout << "mise a jour du deplacement" << endl;
    		cout_mutex.unlock();
        this->rov->updateWithJSON(mainMap["Input"]);
        if (mainMap["Input"][0] == 'x')
        {
          if (this->rov!=nullptr)
          {
            delete this->rov;
            this->rov=nullptr;
          }
        }
        break;
      }

    case 4: // ordreMission du groupe 5
      ordreMission = mainMap["type"];
      if (odmGr5==nullptr)
        {
          odmGr5 = new RapportMission("GR5", mainMap["id_mission"], mainMap["lieu"]);
        }
      else
      cout << "Erreur nouvelle mission, mission en cours" << endl;
	    break;

    case 5: // ordreMission du groupe 7
      ordreMission = mainMap["Instructions"];
      if (odmGr7==nullptr)
        {
          odmGr7 = new RapportMission("GR7", mainMap["ID_ODM"], "generique");
        }
      else
      {
		    	cout_mutex.lock();
		      cout << "Erreur nouvelle mission, mission en cours" << endl;
		    	cout_mutex.unlock();
      }
	    break;
    case 6:
      // gestion des inputs pour envoi de mise a jour RDM
      // Cloture du RDM
      break;
    default:
		  cout_mutex.lock();
      cout << "No corresponding Key" << endl;
    	cout_mutex.unlock();
      break;
    }

    // Fermeture du thread
    this->activeThreads[pos].detach();
    cout_mutex.lock();
    cout << "[INFO] THREAD TERMINATED" << endl;
    cout_mutex.unlock();
}

//! \fn map<string,string> Server::decode(const char* buffIn)
//! \brief fonction de mappage de la donnee JSON
map<string,string> Server::decode(const char* buffIn)
{
    map<string, string> dicoMap;
    cJSON *ptrJSON = cJSON_CreateObject();
    ptrJSON = cJSON_Parse(buffIn);

    // Macro a utiliser pour incrementer le Json
    const cJSON *item = NULL;
    cJSON_ArrayForEach(item, ptrJSON)
    {
      // cout << "string: " << item->string << endl;
      // cout << "valuestring: " << item->valuestring << endl;
      dicoMap.emplace(std::make_pair(item->string, item->valuestring));
    }

    if(ptrJSON!=nullptr)
    {
        cJSON_Delete(ptrJSON);
        // delete ptrJSON;
        ptrJSON = nullptr;
    }
    return dicoMap;

}

