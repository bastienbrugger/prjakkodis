#ifndef RAPPORTMISSION_H
#define RAPPORTMISSION_H

#include "donnee.h"


class RapportMission: public Donnee
{
    public:
        RapportMission(string, string, string);
        ~RapportMission();

        void Setcode(string val) {code = val;}
        string Getcode() {return code;}
        void Setcommandement(string val) {commandement = val;}
        string Getcommandement() {return commandement;}
        void SetId(string val) {Id = val;}
        string GetId() {return Id;}
        void Setetat(string val) {etat = val;}
        string Getetat() {return etat;}
        void Setzone(string val) {zone = val;}
        string Getzone() {return zone;}
        void Setmessage(string val) {message = val;}
        string Getmessage() {return message;}
        
        void Sethorodatage() {horodatage = time(NULL);}
        string Gethorodatage();
        
        
        virtual void updateWithJSON(string) override;
        virtual string maDonneeToJSON() override;
        virtual void logRecord() override;
        virtual void execute() override;
    
    private:
      string code;
      string commandement;
      string Id;
      string etat;
      string message;
      string zone;
      time_t horodatage;
      
};

#endif // RAPPORTMISSION_H
