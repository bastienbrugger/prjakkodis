# Projet AKKODIS

D�veloppement du logiciel embarqu� d'un rover de r�lev�s au sein d'un r�seau de rovers d�di�s � une mission de d�minage 

## Utilisation du makefile

- Compiler le programme : `make`
- Nettoyer les fichiers compiles : `make clean`
- Nettoyer le projet : `make mrproper`

## Ex�cution du programme

- Sur le rover (Raspberry Pi) : `./apps S`
- Sur le poste de pilotage (Ubuntu) : `./apps C`