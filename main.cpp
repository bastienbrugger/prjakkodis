// Bibliotheques de base
#include <iostream>

// Bibliotheques maison
#include "lib/server.h"
#include "lib/inputterminal.h"

// Espaces de noms
using namespace std;

// Corps du programme
//! \brief Lancement du programme:
//! \brief - avec argument S pour lancer sur le Rover (cree le serveur)
//! \brief - avec argument C pour lancer sur le poste de pilotage (cree le serveur et le terminal)
int main(int argc, char *argv[])
{
    // Mauvais appel du programme
    if (argc != 2)
    {
        cout << "Usage: programme <char>" << endl;
        return 1;
    }

    // Lancement cote rover
    else if (*argv[1] == 'S')
    {
        Server soc(8080);
        soc.ecoute();
    }

    // Lancement cote pilotage
    else if (*argv[1] == 'C')
    {
        Server soc(8080);
        inputTerminal IHM;
        thread servThread, termThread;

        cout.rdbuf(IHM.oss.rdbuf());
        servThread = thread(&Server::ecoute, &soc);
        termThread = thread(&inputTerminal::launchIHM, &IHM);
        servThread.join();
        termThread.join();
    }

    // Mauvais appel du programme
    else
    {
        cout << "<char>: S pour Serveur, C pour Client" << endl;
        return 1;
    }

    // Fin du programme
    return 0;
}
