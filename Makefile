###################################
#	MAKEFILE POUR LE PROJET AKKODIS #
###################################



# CONSTANTES
# ----------

# Compilateur
CC = g++

# Nom de l'executable
EXEC = apps

# Options de compilation
CFLAGS = -O2 -lwiringPi -lncurses -lpthread #-g3 -Wall

# Fichiers entetes .h
HEA = $(wildcard lib/*.h)

# Fichiers sources .c et fichiers compiles .o correspondants
SRC_c = $(wildcard lib/*.c)
OBJ_c = $(addprefix obj/, $(SRC_c:.c=.o))

# Fichiers sources .cpp et fichiers compiles .o correspondants
SRC_cpp = $(wildcard lib/*.cpp)
OBJ_cpp = $(addprefix obj/, $(SRC_cpp:.cpp=.o))

# Fichiers sources de la racine et fichiers compiles .o correspondants
SRC = $(wildcard *.cpp)
OBJ = $(addprefix obj/, $(SRC:.cpp=.o))

# Suppression
SUPP = rm -rf



# COMMANDES SHELL
# ----------

# Creation des dossiers de fichiers compiles
$(shell mkdir -p obj/lib)



# COMMANDES MAKE
# ----------

# Compilation complete du programme
all : $(EXEC)

# Generation de l'executable issu de tous les .o generes precedemment
$(EXEC) : $(OBJ_c) $(OBJ_cpp) $(OBJ)
	$(CC)  -o $(EXEC) $^ $(CFLAGS)

# Generation des .o a partir des .c des bibliotheques
obj/lib/%.o : lib/%.c
	$(CC) -o $@ -c $< $(CFLAGS)
 
# Generation des .o a partir des .cpp	des bibliotheques
obj/lib/%.o : lib/%.cpp
	$(CC) -o $@ -c $< $(CFLAGS)
 
# Generation des .o a partir des .cpp de la racine
obj/%.o : %.cpp
	$(CC) -o $@ -c $< $(CFLAGS)

# Nettoyage des fichiers compiles
clean :
	$(SUPP) obj

# Nettoyage complet
mrproper : clean
	$(SUPP) $(EXEC)
	$(SUPP) *.log

# Cration d'une archive
zip : 
	tar -a -c -f prj.zip Makefile README.md $(SRC_cpp) $(SRC_c) $(SRC) $(HEA)
